﻿using System.Collections;
using System.Collections.Generic;

using utils;

namespace collections_printer_csharp {

    class Program {

        static void Main(string[] args) {
            var array0 = new string[3];
            array0[0] = "1";
            array0[1] = "2";
            array0[2] = "3";
            Printer.print(array0);
            
            var array1 = new ArrayList();
            array1.Add(1);
            array1.Add(2);
            array1.Add(3);
            Printer.print(array1);

            var array2 = new List<int>();
            array2.Add(1);
            array2.Add(2);
            array2.Add(3);
            Printer.print(array2);

            var array3 = new Queue();
            array3.Enqueue(1);
            array3.Enqueue(2);
            array3.Enqueue(3);
            array3.Dequeue();
            Printer.print(array3);

            var array4 = new Queue<int>();
            array4.Enqueue(1);
            array4.Enqueue(2);
            array4.Enqueue(3);
            array4.Dequeue();
            Printer.print(array4);

            var array5 = new Stack();
            array5.Push(1);
            array5.Push(2);
            array5.Push(3);
            array5.Pop();
            Printer.print(array5);

            var array6 = new Stack<string>();
            array6.Push("1");
            array6.Push("2");
            array6.Push("3");
            array6.Pop();
            Printer.print(array6);

            var hashtable = new Hashtable();
            hashtable.Add(1, "Abacaxi");
            hashtable.Add(2, "Banana");
            hashtable.Add(3, "Laranja");
            Printer.print(hashtable);

            var dictionary = new Dictionary<string, string>();
            dictionary.Add("1", "Ferrari");
            dictionary.Add("2", "Lamborghini");
            dictionary.Add("3", "Mercedes");
            Printer.print<string>(dictionary);

            var sortedList = new SortedList<string, string>();
            sortedList.Add("1", "Ferrari");
            sortedList.Add("2", "Lamborghini");
            sortedList.Add("3", "Mercedes");
            Printer.print<string>(sortedList);
        }
    }
}
