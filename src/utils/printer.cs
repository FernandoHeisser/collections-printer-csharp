using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace utils {

    public class Printer {

        public static void print<T>(params T[] array) {
            var s = "[";
            foreach(var e in array) {
                s += e;
                if(!array[array.Length-1].Equals(e)) {
                    s += ", ";
                }
            }
            s += "]";
            Console.WriteLine(s);
        }
        
        public static void print(ArrayList array) {
            var s = "[";
            foreach(var e in array) {
                s += e;
                if(array[array.Count-1] != e) {
                    s += ", ";
                }
            }
            s += "]";
            Console.WriteLine(s);
        }

        public static void print<T>(List<T> list) {
            var s = "[";
            foreach(var e in list) {
                s += e;
                if(!list[list.Count-1].Equals(e)) {
                    s += ", ";
                }
            }
            s += "]";
            Console.WriteLine(s);
        }

        public static void print<T>(Queue<T> queue) {
            var list = queue.ToList();
            var s = "[";
            foreach(var e in list) {
                s += e;
                if(!list[list.Count-1].Equals(e)) {
                    s += ", ";
                }
            }
            s += "]";
            Console.WriteLine(s);
        }
        
        public static void print(Queue queue) {
            var list = queue.ToArray();
            var s = "[";
            foreach(var e in list) {
                s += e;
                if(!list[list.Length-1].Equals(e)) {
                    s += ", ";
                }
            }
            s += "]";
            Console.WriteLine(s);
        }

        public static void print(Stack stack) {
            var list = stack.ToArray();
            var s = "[";
            foreach(var e in list) {
                s += e;
                if(!list[list.Length-1].Equals(e)) {
                    s += ", ";
                }
            }
            s += "]";
            Console.WriteLine(s);
        }

        public static void print<T>(Stack<T> stack) {
            var list = stack.ToList();
            var s = "[";
            foreach(var e in list) {
                s += e;
                if(!list[list.Count-1].Equals(e)) {
                    s += ", ";
                }
            }
            s += "]";
            Console.WriteLine(s);
        }

        public static void print(Hashtable hashtable) {
            var s = "[";
            foreach(DictionaryEntry e in hashtable) {
                s += e.Key;
                s += ": ";
                s += e.Value;
                s += ", ";
            }
            s = s.Remove(s.Length-1);
            s = s.Remove(s.Length-1);
            s += "]";
            Console.WriteLine(s);
        }

        public static void print<T>(Dictionary<T,T> dictionary) {
            var s = "[";
            foreach(var e in dictionary) {
                s += e.Key;
                s += ": ";
                s += e.Value;
                s += ", ";
            }
            s = s.Remove(s.Length-1);
            s = s.Remove(s.Length-1);
            s += "]";
            Console.WriteLine(s);
        }

        public static void print<T>(SortedList<T,T> sortedList) {
            var s = "[";
            foreach(var e in sortedList) {
                s += e.Key;
                s += ": ";
                s += e.Value;
                s += ", ";
            }
            s = s.Remove(s.Length-1);
            s = s.Remove(s.Length-1);
            s += "]";
            Console.WriteLine(s);
        }
    }
}
